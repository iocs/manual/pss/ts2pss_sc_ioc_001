# Startup for KG-GTA:PSS-IOC-1

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/kg_gta_pss_gplc_1.iocsh", "DBDIR=$(E3_CMD_TOP)/db/, MODVERSION=$(IOCVERSION=)")

